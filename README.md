# pathadvisor-backend

Backend APIs for pathadvisor

For API documentation, please see here.

### Start the server in development environment
`npm run dev`

### Technology stack
Express and node.js for the API server.

MongoDB for persistence layer

Native mongodb driver for node.js.

### Environment file
Environment variables like database connection string will be loaded from `.env*` file.

The server will load the following .env files with more priority for the files on the top than at the bottom.

1. .env.[NODE_ENV].local
2. .env.[NODE_ENV]
3. .env.local (omitted if NODE_ENV=test)
4. .env

You can also supply environment variable during start up.

`ENV1=sample npm run start`

OS environment variables and environment variables supplied in terminal will always takes precedence than the variables in `.env*` file.

### Project structure

#### index.js
Entry point

#### db/*
Database connection handlers

#### routers/*
API endpoints handlers

#### models/*
Entity abstraction. CRUD methods and schema for entities.

#### graph/*
Graph algorithm related files

#### logger/*
Logger model

#### errors
Custom error object

#### responses
Response object

Every http json output should be wrapped by successResponse or errorResponse

### Public cli scripts

Create database indexes. Only need to execute for the once unless entity schema is changed.
`npm run createIndexes`

Integration test
`npm run test`
