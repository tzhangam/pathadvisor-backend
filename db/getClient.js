'use strict';

const { MongoClient } = require('mongodb');

let mongoClient;

function connectDB() {
  return MongoClient.connect(
    process.env.DB_URL,
    { useNewUrlParser: true },
  );
}

module.exports = async function getClient() {
  if (!mongoClient) {
    mongoClient = await connectDB();
  }
  return mongoClient;
};
