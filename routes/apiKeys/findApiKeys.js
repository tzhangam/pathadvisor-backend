'use strict';

const successResponse = require('../../responses/successResponse');
const apiKeys = require('../../models/apiKeys');

async function findApiKeys(req, res) {
  const data = await (await apiKeys.find({}, { projection: { key: 0 } })).toArray();
  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findApiKeys;
