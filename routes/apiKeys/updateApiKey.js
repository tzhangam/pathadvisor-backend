'use strict';

const Ajv = require('ajv');
const successResponse = require('../../responses/successResponse');
const ValidationError = require('../../errors/ValidationError');
const apiKeys = require('../../models/apiKeys');
const ObjectId = require('../../utils/ObjectId');
const permissions = require('../../auth/permissions');

async function updateApiKey(req, res) {
  const _id = ObjectId(req.params.id);
  const timestamp = new Date().getTime();

  const requestBodySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    minProperties: 1,
    properties: {
      namespace: {
        $id: '#/properties/namespace',
        type: 'string',
        minLength: 1,
      },
      expiryAt: {
        $id: '#/properties/expiryAt',
        type: ['integer', 'null'],
      },
      inactive: {
        $id: '#/properties/inactive',
        type: 'boolean',
      },
      permissions: {
        $id: '#/properties/permissions',
        type: 'array',
        items: {
          type: 'string',
          // currently only allow api keys to have this permission
          enum: [permissions['NODE:UPDATE:OTHERS']],
        },
      },
    },
  };

  const validator = new Ajv({ allErrors: true });
  const validate = validator.compile(requestBodySchema);

  if (!validate(req.body)) {
    throw new ValidationError(validate.errors, 'Validation Error');
  }

  const data = await apiKeys.updateOne(
    { _id },
    {
      ...req.body,
      updatedAt: timestamp,
    },
  );

  res.json(successResponse({ data }));
}

module.exports = updateApiKey;
