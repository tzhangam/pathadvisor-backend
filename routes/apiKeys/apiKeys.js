'use strict';

const router = require('express-promise-router')();
const findApiKeys = require('./findApiKeys');
const insertApiKey = require('./insertApiKey');
const updateApiKey = require('./updateApiKey');
const deleteApiKey = require('./deleteApiKey');

const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/apiKeys', checkPermissions([permissions['APIKEY:LIST']]), findApiKeys);
router.post('/apiKeys', checkPermissions([permissions['APIKEY:INSERT']]), insertApiKey);
router.post('/apiKeys/:id', checkPermissions([permissions['APIKEY:UPDATE']]), updateApiKey);
router.delete('/apiKeys/:id', checkPermissions([permissions['APIKEY:DELETE']]), deleteApiKey);

module.exports = router;
