'use strict';

const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const floors = require('../../models/floors');
const nodes = require('../../models/nodes');

async function deleteFloor(req, res) {
  const _id = req.params.id;
  const node = await nodes.findOne({ floorId: _id });

  if (node) {
    res.status(400).json(
      errorResponse({
        message: 'Cannot delete target floor because it is connected to one or more nodes',
      }),
    );
    return;
  }

  await floors.deleteOne({ _id });
  res.json(successResponse({ meta: { deleted: true } }));
}

module.exports = deleteFloor;
