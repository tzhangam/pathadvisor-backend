'use strict';

const Ajv = require('ajv');
const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const suggestions = require('../../models/suggestions');
const ObjectId = require('../../utils/ObjectId');

async function resolveSuggestion(req, res) {
  const requestBodySchema = {
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'boolean',
  };

  const validator = new Ajv({ allErrors: true });
  const validate = validator.compile(requestBodySchema);

  if (!validate(req.body)) {
    res.status(400).json(
      errorResponse({
        message: 'Request body validation errors',
        validationErrors: validate.errors,
      }),
    );

    return;
  }

  const data = await suggestions.updateOne(
    { _id: new ObjectId(req.params.id) },
    { resolved: req.body, updatedAt: new Date().getTime() },
  );
  res.json(successResponse({ data }));
}

module.exports = resolveSuggestion;
