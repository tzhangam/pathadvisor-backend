'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const nodes = require('../../models/nodes');
const errorResponse = require('../../responses/errorResponse');
const successResponse = require('../../responses/successResponse');
const pointInPolygon = require('./pointInPolygon');

async function findNodeNearCoordinates(req, res) {
  const { floorId } = req.params;
  const { nearCoordinates } = req.query;

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['floorId', 'nearCoordinates'],
    properties: {
      floorId: {
        $id: '#/properties/floorId',
        type: 'string',
        minLength: 1,
      },
      nearCoordinates: {
        $id: '#/properties/nearCoordinates',
        type: 'string',
        pattern: '^[0-9]+,[0-9]+$',
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ floorId, nearCoordinates })) {
    res.status(400).json(
      errorResponse({
        message: 'Query string validation errors',
        validationErrors: validate.errors,
      }),
    );

    return;
  }

  const point = nearCoordinates.split(',').map(v => Number.parseInt(v, 10));

  const MAX_NEAR_DISTANCE = 200;

  let data;
  data = await nodes.findOne(
    {
      floorId,
      polygonCoordinates: { $near: point, $maxDistance: MAX_NEAR_DISTANCE },
    },
    { projection: { polygonCoordinates: 0 } },
  );

  let inPolygon = false;

  if (data && data.geoLocs && data.geoLocs.type === 'MultiPolygon') {
    inPolygon = data.geoLocs.coordinates.some(([outerPolygon]) =>
      pointInPolygon(point, outerPolygon),
    );
  }

  if (!inPolygon) {
    // find nearest node (point) if the point is not in any polygon
    data = await nodes.findOne(
      {
        floorId,
        coordinates: { $near: point, $maxDistance: MAX_NEAR_DISTANCE },
      },
      { projection: { polygonCoordinates: 0 } },
    );
  }

  res.json(
    successResponse({
      data,
    }),
  );
}

module.exports = findNodeNearCoordinates;
