'use strict';

const router = require('express-promise-router')();

const findNodes = require('./findNodes');
const findNodesByKeyword = require('./findNodesByKeyword');
const findNodesWithinBox = require('./findNodesWithinBox');
const findNodeNearCoordinates = require('./findNodeNearCoordinates');
const insertNode = require('./insertNode');
const updateNode = require('./updateNode');
const updateNodeOthers = require('./updateNodeOthers');
const removeNode = require('./deleteNode');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/nodes', findNodesByKeyword);
router.get(
  '/floors/:floorId/nodes',
  async (req, res, next) => {
    if (Object.keys(req.query).length === 0) {
      await checkPermissions([permissions['NODE:LIST']])(req, res, next);
      return;
    }
    next();
  },
  async (req, res) => {
    if (Object.keys(req.query).length === 0) {
      await findNodes(req, res);
      return;
    }
    await findNodesWithinBox(req, res);
  },
);
router.get('/floors/:floorId/node', findNodeNearCoordinates);
router.post('/nodes', checkPermissions([permissions['NODE:INSERT']]), insertNode);
router.post('/nodes/:id', checkPermissions([permissions['NODE:UPDATE']]), updateNode);
router.post(
  '/nodes/:id/others',
  checkPermissions([permissions['NODE:UPDATE:OTHERS']]),
  updateNodeOthers,
);
router.delete('/nodes/:id', checkPermissions([permissions['NODE:DELETE']]), removeNode);

module.exports = router;
