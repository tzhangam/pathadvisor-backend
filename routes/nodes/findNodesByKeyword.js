'use strict';

const nodes = require('../../models/nodes');
const errorResponse = require('../../responses/errorResponse');
const successResponse = require('../../responses/successResponse');

async function findNodesByKeyword(req, res) {
  const MAX_NODES_RETURN_LIMIT = 10;

  const { name = '' } = req.query || {};
  const cleaned = { name: name.trim() };

  const query = {};

  if (cleaned.name) {
    query.$or = [
      {
        name: {
          $regex: cleaned.name,
          $options: 'i',
        },
      },
      {
        keywords: {
          $regex: cleaned.name,
          $options: 'i',
        },
      },
    ];
  }

  if (Object.keys(query).length === 0) {
    res.status(400).json(
      errorResponse({
        message: 'Empty query is not allowed',
      }),
    );

    return;
  }

  query.unsearchable = null;

  const cursor = await nodes.find(query, {
    limit: MAX_NODES_RETURN_LIMIT,
    projection: { polygonCoordinates: 0 },
  });

  const data = await cursor.toArray();

  res.json(
    successResponse({
      data,
      meta: { count: data.length },
    }),
  );
}

module.exports = findNodesByKeyword;
