'use strict';

const successResponse = require('../../responses/successResponse');
const nodes = require('../../models/nodes');

async function updateNodeOthers(req, res) {
  const _id = req.params.id;
  const { namespace } = req;

  if (!namespace) {
    throw new Error('namespace cannot be null');
  }

  await nodes.updateOneNoValidate({ _id }, { $set: { [`others.${namespace}`]: req.body } });
  res.json(successResponse({ data: req.body }));
}

module.exports = updateNodeOthers;
