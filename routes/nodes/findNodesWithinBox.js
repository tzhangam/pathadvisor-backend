'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const keyBy = require('lodash.keyby');
const nodes = require('../../models/nodes');
const connectors = require('../../models/connectors');
const errorResponse = require('../../responses/errorResponse');
const successResponse = require('../../responses/successResponse');

async function findNodesWithinBox(req, res) {
  const MAX_NODES_RETURN_LIMIT = 50;
  const { floorId } = req.params;
  const { boxCoordinates } = req.query;

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['floorId'],
    properties: {
      floorId: {
        $id: '#/properties/floorId',
        type: 'string',
        minLength: 1,
      },
      boxCoordinates: {
        $id: '#/properties/boxCoordinates',
        type: 'string',
        pattern: '^[0-9]+,[0-9]+,[0-9]+,[0-9]+$',
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ floorId, boxCoordinates })) {
    res.status(400).json(
      errorResponse({
        message: 'Query string validation errors',
        validationErrors: validate.errors,
      }),
    );

    return;
  }

  // TODO: allow for admin
  if (!boxCoordinates) {
    res.status(400).json(
      errorResponse({
        message: 'Empty query is not allowed',
      }),
    );
    return;
  }

  const query = { floorId };

  if (boxCoordinates) {
    const [x1, y1, x2, y2] = boxCoordinates.split(',').map(v => Number.parseInt(v, 10));
    query.polygonCoordinates = { $geoWithin: { $box: [[x1, y1], [x2, y2]] } };
  }

  const cursor = await nodes.find(query, {
    limit: MAX_NODES_RETURN_LIMIT,
    projection: { polygonCoordinates: 0 },
  });

  const indexesByConnectorId = {};
  const data = await cursor.toArray();

  data.forEach(({ connectorId }, i) => {
    if (!connectorId) {
      return;
    }

    indexesByConnectorId[connectorId] = indexesByConnectorId[connectorId]
      ? [...indexesByConnectorId[connectorId], i]
      : [i];
  });

  const connectorIds = Object.keys(indexesByConnectorId);

  if (connectorIds.length) {
    const connectorsById = keyBy(
      await (await connectors.find(
        { _id: { $in: connectorIds } },
        {
          projection: { tagIds: 1 },
        },
      )).toArray(),
      o => o._id,
    );

    // Concat tagIds from connectors to nodes
    Object.keys(indexesByConnectorId).forEach(connectorId => {
      const dataIndexes = indexesByConnectorId[connectorId];
      dataIndexes.forEach(dataIndex => {
        data[dataIndex].tagIds = data[dataIndex].tagIds
          ? [...data[dataIndex].tagIds, ...connectorsById[connectorId].tagIds]
          : connectorsById[connectorId].tagIds;
      });
    });
  }

  res.json(
    successResponse({
      data,
      meta: { count: data.length },
    }),
  );
}

module.exports = findNodesWithinBox;
