'use strict';

const nodes = require('../../models/nodes');
const successResponse = require('../../responses/successResponse');

async function findNodes(req, res) {
  const { floorId } = req.params;
  const data = await (await nodes.find(
    { floorId },
    { projection: { polygonCoordinates: 0 } },
  )).toArray();

  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findNodes;
