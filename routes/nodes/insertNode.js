'use strict';

const shortid = require('shortid');
const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const nodes = require('../../models/nodes');
const connectors = require('../../models/connectors');
const floors = require('../../models/floors');

async function insertNode(req, res) {
  const { connectorId, floorId } = req.body;

  if (connectorId) {
    const connector = await connectors.findOne({ _id: connectorId });
    if (!connector) {
      res.status(400).json(errorResponse({ message: `connector ${connectorId} not found` }));
      return;
    }
  }

  if (floorId) {
    const floor = await floors.findOne({ _id: floorId });
    if (!floor) {
      res.status(400).json(errorResponse({ message: `floor ${floorId} not found` }));
      return;
    }
  }

  const _id = shortid();
  const data = await nodes.insertOne({ ...req.body, _id });
  res.json(successResponse({ data }));
}

module.exports = insertNode;
