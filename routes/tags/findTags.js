'use strict';

const successResponse = require('../../responses/successResponse');
const tags = require('../../models/tags');

async function findTags(req, res) {
  const data = await (await tags.find()).toArray();
  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findTags;
