'use strict';

const successResponse = require('../../responses/successResponse');
const tags = require('../../models/tags');
const NotFoundError = require('../../errors/NotFoundError');

async function upsertTag(req, res) {
  const _id = req.params.id;

  try {
    const data = await tags.updateOne({ _id }, req.body);
    res.json(successResponse({ data }));
  } catch (error) {
    if (!(error instanceof NotFoundError)) {
      throw error;
    }

    // insert new data if not found one
    const data = await tags.insertOne({ ...req.body, _id });
    res.json(successResponse({ data }));
  }
}

module.exports = upsertTag;
