'use strict';

const router = require('express-promise-router')();
const findTags = require('./findTags');
const upsertTag = require('./upsertTag');
const deleteTag = require('./deleteTag');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/tags', findTags);
router.post(
  '/tags/:id',
  checkPermissions([permissions['TAG:INSERT'], permissions['TAG:UPDATE']]),
  upsertTag,
);
router.delete('/tags/:id', checkPermissions([permissions['TAG:DELETE']]), deleteTag);

module.exports = router;
