'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const buildGraph = require('../graph/buildGraph');

const router = express.Router();

const nodes = require('./nodes/nodes');
const connectors = require('./connectors/connectors');
const search = require('./search/search');
const settings = require('./settings/settings');
const floors = require('./floors/floors');
const tags = require('./tags/tags');
const edges = require('./edges/edges');
const logs = require('./logs/logs');
const suggestions = require('./suggestions/suggestions');
const auth = require('./auth/auth');
const apiKeys = require('./apiKeys/apiKeys');

router.use(bodyParser.json({ strict: false }));

router.use(nodes);
router.use(connectors);
router.use(settings);
router.use(floors);
router.use(tags);
router.use(edges);
router.use(logs);
router.use(suggestions);
router.use(auth);
router.use(apiKeys);

let cached = null;
router.use(async (req, res, next) => {
  if (!cached) {
    const { graph, nodesById, connectorsById } = await buildGraph();
    cached = { graph, nodesById, connectorsById };
  }

  req.graph = cached.graph;
  req.nodesById = cached.nodesById;
  req.connectorsById = cached.connectorsById;

  next();
});
router.use(search);

module.exports = router;
