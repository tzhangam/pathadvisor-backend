'use strict';

const successResponse = require('../../responses/successResponse');
const settings = require('../../models/settings');

async function insertSettings(req, res, next) {
  const data = await settings.insertOne(req.body);
  res.json(successResponse({ data }));
}

module.exports = insertSettings;
