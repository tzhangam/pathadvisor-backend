'use strict';

const sessions = require('../../models/sessions');
const successResponse = require('../../responses/successResponse');
const { SESSION_KEY_HEADER } = require('../../auth/auth');

async function logout(req, res) {
  const session = req.header(SESSION_KEY_HEADER);
  await sessions.deleteOne({ _id: session });
  res.json(successResponse({ data: { message: 'Logged out successfully' } }));
}

module.exports = logout;
