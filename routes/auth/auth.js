'use strict';

const router = require('express-promise-router')();

const login = require('./login');
const logout = require('./logout');

router.post('/login', login);
router.post('/logout', logout);

module.exports = router;
