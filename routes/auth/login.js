'use strict';

const users = require('../../models/users');
const { authenticateUser, REASONS, createSession } = require('../../auth/auth');
const successResponse = require('../../responses/successResponse');
const AuthError = require('../../errors/AuthError');
const logger = require('../../logger/logger');

async function login(req, res) {
  const { username, password } = req.body;
  const { error } = await authenticateUser(username, password);

  const timestamp = new Date().getTime();

  if (error === REASONS.INVALID_PASSWORD) {
    await users.updateOneNoValidate({ _id: username }, { $inc: { failedAttempts: 1 } });
    logger.info({
      label: 'auth',
      message: `failedAttempts for ${username} increased by 1`,
    });
  }

  if (error) {
    throw new AuthError(
      { username, reason: error },
      error === REASONS.LOCKED
        ? 'Account is locked, please contact ITSC for help'
        : 'Failed to authenticate user',
    );
  }

  const { error: sessionError, session } = await createSession(username);

  if (sessionError) {
    throw new AuthError(
      { username, reason: sessionError },
      sessionError === REASONS.MAX_SESSION_REACHED
        ? 'Account has reached the max number of sessions allowed'
        : 'Failed to authenticate user',
    );
  }

  await users.updateOne({ _id: username }, { lastLoginAt: timestamp });

  logger.info({
    label: 'auth',
    message: `login successful for user ${username} and session key has been assigned`,
  });

  res.json(successResponse({ data: { session } }));
}

module.exports = login;
