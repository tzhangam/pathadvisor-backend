'use strict';

const router = require('express-promise-router')();
const findEdges = require('./findEdges');
const insertEdge = require('./insertEdge');
const deleteEdge = require('./deleteEdge');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/floors/:floorId/edges', checkPermissions([permissions['EDGE:LIST']]), findEdges);
router.post('/edges', checkPermissions([permissions['EDGE:INSERT']]), insertEdge);
router.delete('/edges/:id', checkPermissions([permissions['EDGE:DELETE']]), deleteEdge);

module.exports = router;
