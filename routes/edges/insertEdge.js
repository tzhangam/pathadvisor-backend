'use strict';

const shortid = require('shortid');
const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const nodes = require('../../models/nodes');
const edges = require('../../models/edges');

async function insertEdge(req, res) {
  const { fromNodeId, toNodeId } = req.body;
  // To suppress json schema validation output to client saying missing floorId
  let floorId = 'DEFAULT';

  if (fromNodeId && toNodeId) {
    const [fromNode, toNode] = await (await nodes.find(
      {
        _id: { $in: [fromNodeId, toNodeId] },
      },
      { projection: { floorId: 1 } },
    )).toArray();

    if (!fromNode || !toNode || fromNode.floorId !== toNode.floorId) {
      res.status(400).json(
        errorResponse({
          message:
            'Cannot insert edge because fromNodeId or toNodeId not found or they are have different floorId',
        }),
      );
      return;
    }

    floorId = fromNode.floorId;
  }

  const _id = shortid();

  const data = await edges.insertOne({ ...req.body, _id, floorId });
  res.json(successResponse({ data }));
}

module.exports = insertEdge;
