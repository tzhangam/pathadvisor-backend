'use strict';

const successResponse = require('../../responses/successResponse');
const connectors = require('../../models/connectors');
const NotFoundError = require('../../errors/NotFoundError');

async function upsertConnector(req, res) {
  const _id = req.params.id;

  try {
    const data = await connectors.updateOne({ _id }, req.body);
    res.json(successResponse({ data }));
  } catch (error) {
    if (!(error instanceof NotFoundError)) {
      throw error;
    }

    // insert new data if not found one
    const data = await connectors.insertOne({ ...req.body, _id });
    res.json(successResponse({ data }));
  }
}

module.exports = upsertConnector;
