'use strict';

const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const connectors = require('../../models/connectors');
const nodes = require('../../models/nodes');

async function deleteConnector(req, res) {
  const _id = req.params.id;

  const node = await nodes.findOne({ connectorId: _id });

  if (node) {
    res.status(400).json(
      errorResponse({
        message: `Cannot delete target connector because it is connected by one or more nodes`,
      }),
    );

    return;
  }

  await connectors.deleteOne({ _id });
  res.json(successResponse({ meta: { deleted: true } }));
}

module.exports = deleteConnector;
