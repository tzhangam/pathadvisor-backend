'use strict';

const router = require('express-promise-router')();
const insertLog = require('./insertLog');

router.post('/logs', insertLog);
module.exports = router;
