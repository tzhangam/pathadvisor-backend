'use strict';

const successResponse = require('../../responses/successResponse');
const logs = require('../../models/logs');

async function insertLog(req, res) {
  const data = await logs.insertOne({
    ...req.body,
    datetime: new Date().getTime(),
    ipAddress: req.headers['x-forwarded-for'] || req.ip,
    referer: req.header('referer'),
    userAgent: req.header('user-agent'),
  });
  res.json(successResponse({ data }));
}

module.exports = insertLog;
