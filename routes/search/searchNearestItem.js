'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });
const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const nodeModel = require('../../models/nodes');

const convertLiteralToBoolean = require('../../utils/convertLiteralToBoolean');
const breadthFirstSearch = require('../../graph/breadthFirstSearch');
const { INF } = require('../../graph/constants');

const querySchema = {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  required: ['type'],
  properties: {
    startNodeId: {
      $id: '#/properties/startNodeId',
      type: 'string',
      minLength: 1,
    },
    sameFloor: {
      $id: '#/properties/sameFloor',
      type: 'string',
      enum: ['true', 'false'],
    },
    type: {
      $id: '#/properties/type',
      type: 'string',
      minLength: 1,
    },
  },
};

const validate = validator.compile(querySchema);

async function searchNearestItem(req, res) {
  const { graph, nodesById, connectorsById } = req;
  const sendNoItemFoundError = () => {
    res.status(404).json(errorResponse({ message: 'No nearest item found' }));
  };
  if (!graph) {
    throw new Error('No graph is found');
  }

  if (!nodesById) {
    throw new Error('nodesById not found');
  }

  if (!connectorsById) {
    throw new Error('connectorsById not found');
  }

  const { startNodeId } = req.params;
  const { type } = req.query;
  let { sameFloor } = req.query;

  if (!validate({ startNodeId, type, sameFloor })) {
    res.status(400).json(
      errorResponse({
        message: 'Query string validation errors',
        validationErrors: validate.errors,
      }),
    );

    return;
  }

  sameFloor = sameFloor ? convertLiteralToBoolean(sameFloor) : true;

  if (!graph[startNodeId]) {
    sendNoItemFoundError();
    return;
  }

  const startNodeFloor = nodesById[startNodeId].floorId;

  const { prev, dist } = breadthFirstSearch(graph, startNodeId, ({ nodeId, weight }) => {
    if (sameFloor && nodesById[nodeId].floorId !== startNodeFloor) {
      return {
        shouldSkip: true,
      };
    }

    return { weight };
  });

  let minDist = INF;
  let nearestNodeId = null;

  Object.keys(prev).forEach(nodeId => {
    if (nodeId === startNodeId) {
      return;
    }

    if (nodesById[nodeId].tags[type] && dist[nodeId] < minDist) {
      minDist = dist[nodeId];
      nearestNodeId = nodeId;
    }
  });

  if (!nearestNodeId) {
    sendNoItemFoundError();
    return;
  }

  const data = await nodeModel.findOne(
    { _id: nearestNodeId },
    { projection: { polygonCoordinates: 0 } },
  );
  res.status(200).json(successResponse({ data }));
}

module.exports = searchNearestItem;
