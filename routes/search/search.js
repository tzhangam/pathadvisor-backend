'use strict';

const router = require('express-promise-router')();

const searchShortestPath = require('./searchShortestPath');
const searchNearestItem = require('./searchNearestItem');

router.get('/shortest-path', searchShortestPath);
router.get('/nodes/:startNodeId/nearest-item', searchNearestItem);

module.exports = router;
