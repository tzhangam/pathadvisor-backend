'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const keyBy = require('lodash.keyby');
const nodeModel = require('../../models/nodes');

const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const breadthFirstSearch = require('../../graph/breadthFirstSearch');
const convertLiteralToBoolean = require('../../utils/convertLiteralToBoolean');

const { MAX_WEIGHT, CONNECTOR_SMALLEST_WEIGHT } = require('../../graph/constants');

const MODES = {
  SHORTEST_TIME: 'shortestTime',
  SHORTEST_DISTANCE: 'shortestDistance',
  MIN_NO_OF_LIFTS: 'minNoOfLifts',
};

async function searchShortestPath(req, res) {
  const { graph, nodesById, connectorsById } = req;
  const sendPathNotFoundError = () => {
    res.status(404).json(errorResponse({ message: 'Not path found' }));
  };

  if (!graph) {
    throw new Error('No graph is found');
  }

  if (!nodesById) {
    throw new Error('nodesById not found');
  }

  if (!connectorsById) {
    throw new Error('connectorsById not found');
  }

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    required: ['fromId', 'toId'],
    properties: {
      fromId: {
        $id: '#/properties/fromId',
        type: 'string',
        minLength: 1,
      },
      toId: {
        $id: '#/properties/toId',
        type: 'string',
        minLength: 1,
      },
      mode: {
        $id: '#/properties/mode',
        type: 'string',
        enum: Object.values(MODES),
      },
      includeStair: {
        $id: '#/properties/includeStair',
        type: 'string',
        enum: ['true', 'false'],
      },
      includeEscalator: {
        $id: '#/properties/includeEscalator',
        type: 'string',
        enum: ['true', 'false'],
      },
    },
  };

  const validate = validator.compile(querySchema);

  const { fromId, toId, mode = MODES.SHORTEST_TIME } = req.query;
  let { includeStair, includeEscalator } = req.query;

  if (!validate({ fromId, toId, mode, includeStair, includeEscalator })) {
    res.status(400).json(
      errorResponse({
        message: 'Query string validation errors',
        validationErrors: validate.errors,
      }),
    );

    return;
  }

  includeStair = includeStair ? convertLiteralToBoolean(includeStair) : false;
  includeEscalator = includeEscalator ? convertLiteralToBoolean(includeEscalator) : true;

  if (!graph[fromId]) {
    sendPathNotFoundError();
    return;
  }

  const { prev, dist } = breadthFirstSearch(graph, fromId, ({ nodeId, weight }) => {
    const { tags, connectorId } = nodesById[nodeId];

    if (!includeEscalator && tags && tags.escalator) {
      return { shouldSkip: true };
    }

    if (!includeStair && tags && tags.stair) {
      return { shouldSkip: true };
    }

    if (
      mode === MODES.MIN_NO_OF_LIFTS &&
      connectorId &&
      !connectorsById[connectorId].ignoreMinLiftRestriction &&
      (tags.lift || tags.escalator || tags.crossBuildingConnector)
    ) {
      return { weight: MAX_WEIGHT };
    }

    if (mode === MODES.SHORTEST_DISTANCE && connectorId) {
      return { weight: CONNECTOR_SMALLEST_WEIGHT };
    }

    return { weight };
  });

  const pathNodeIds = [toId];
  let prevNodeId = toId;
  while (prev[prevNodeId]) {
    pathNodeIds.push(prev[prevNodeId]);
    prevNodeId = prev[prevNodeId];
  }

  if (prevNodeId !== fromId) {
    sendPathNotFoundError();
    return;
  }

  const nodes = await (await nodeModel.find(
    { _id: { $in: pathNodeIds } },
    { projection: { polygonCoordinates: 0 } },
  )).toArray();

  const fullNodesById = keyBy(nodes, o => o._id);
  const data = pathNodeIds
    .map((nodeId, i) => {
      const nextDist = i + 1 >= pathNodeIds.length ? 0 : dist[pathNodeIds[i + 1]];
      return {
        ...fullNodesById[nodeId],
        distance: dist[nodeId] - nextDist,
      };
    })
    .reverse();
  res.json(successResponse({ data }));
}

module.exports = searchShortestPath;
