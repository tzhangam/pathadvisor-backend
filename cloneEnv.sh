git submodule init
git submodule update
find "./pathadvisor-backend-settings" -maxdepth 1 -print | while read file; do
  if [[ $file != *".git"* ]] && [ $file != "./pathadvisor-backend-settings" ]; then
    ln "$file" .;
  fi
done
