'use strict';

const {
  API_KEY_HEADER,
  SESSION_KEY_HEADER,
  getPermissionsFromSession,
  getPermissionsFromApiKey,
} = require('../auth/auth');
const AuthError = require('../errors/AuthError');

function checkPermissions(requiredPermissions) {
  if (!Array.isArray(requiredPermissions) || requiredPermissions.length === 0) {
    throw new Error('requiredPermissions must be an non-empty array');
  }

  return async (req, res, next) => {
    const apiKey = req.header(API_KEY_HEADER);
    const sessionKey = req.header(SESSION_KEY_HEADER);

    switch (true) {
      case Boolean(apiKey): {
        const { error, permissions, namespace } = await getPermissionsFromApiKey(apiKey);

        if (error) {
          throw new AuthError({ reason: error }, 'Failed to authorize');
        }

        if (!requiredPermissions.every(requiredPermission => permissions[requiredPermission])) {
          throw new AuthError({ reason: 'Not enough permissions' }, 'Failed to authorize');
        }

        req.namespace = namespace;
        next();
        return;
      }

      case Boolean(sessionKey): {
        const { error, permissions } = await getPermissionsFromSession(sessionKey);

        if (error) {
          throw new AuthError({ reason: error }, 'Failed to authorize');
        }

        if (!requiredPermissions.every(requiredPermission => permissions[requiredPermission])) {
          throw new AuthError({ reason: 'Not enough permissions' }, 'Failed to authorize');
        }

        next();
        return;
      }

      default:
        throw new AuthError({ reason: 'No key provided' }, 'Failed to authorize');
    }
  };
}

module.exports = checkPermissions;
