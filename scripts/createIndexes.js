'use strict';

const nodes = require('../models/nodes');
const edges = require('../models/edges');
const apiKeys = require('../models/apiKeys');
const loadEnv = require('../env');

loadEnv();

async function createIndexes() {
  try {
    await nodes.createIndex({
      name: 1,
    });

    await nodes.createIndex({
      keywords: 1,
    });

    // legacy 2d index
    await nodes.createIndex(
      {
        polygonCoordinates: '2d',
        floorId: 1,
      },
      { min: 0, max: 6000 },
    );

    await nodes.createIndex(
      {
        coordinates: '2d',
        floorId: 1,
      },
      { min: 0, max: 6000 },
    );

    await edges.createIndex({ floorId: 1 });

    await apiKeys.createIndex({ key: 1 });
  } catch (err) {
    console.error(err);
  }

  console.log('Done');
  process.exit();
}

createIndexes();
