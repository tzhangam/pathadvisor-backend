'use strict';

const { RAW_DATA_PATH } = process.env;
const settings = require('../../models/settings');

// eslint-disable-next-line
const settingsData = require(`${process.cwd()}/${RAW_DATA_PATH}/settings.js`);

async function migrate() {
  try {
    await settings.insertOne(settingsData);
    console.log(`Inserted settings`);
  } catch (err) {
    console.error(`Error inserting settings`, err);
  }

  console.log('Done');
  process.exit();
}

migrate();
