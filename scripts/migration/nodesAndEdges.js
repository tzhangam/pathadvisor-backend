'use strict';

const fs = require('fs');
const keyBy = require('lodash.keyby');
const shortid = require('shortid');
const compactObject = require('./compactObject');

const { RAW_DATA_PATH } = process.env;

const edgesModel = require('../../models/edges');
const nodesModel = require('../../models/nodes');

// eslint-disable-next-line
const floorsData = require(`${process.cwd()}/${RAW_DATA_PATH}/floors.js`);

function camelCase(str, separator) {
  return str
    .split(separator)
    .map((partStr, i) =>
      i !== 0 && partStr ? partStr[0].toUpperCase() + partStr.slice(1) : partStr,
    )
    .join('');
}

const connectorType = {
  crossBuildingConnector: true,
  escalator: true,
  lift: true,
  stair: true,
};

const deprecatedType = {
  general: true,
  hideFromSearching: true,
};

function getTagIdsFromType(type) {
  if (!type.trim()) {
    return [];
  }

  const id = camelCase(type.trim(), ' ');
  if (connectorType[id] || deprecatedType[id]) {
    return [];
  }

  return [id];
}

async function migrateFloor(floor) {
  const edgeTextContent = fs.readFileSync(`${RAW_DATA_PATH}/edges_${floor}.xml`, {
    encoding: 'utf8',
  });

  const connectorTextContent = fs.readFileSync(`${RAW_DATA_PATH}/connectors_${floor}.xml`, {
    encoding: 'utf8',
  });

  const subAreaTextContent = fs.readFileSync(`${RAW_DATA_PATH}/subArea_${floor}.xml`, {
    encoding: 'utf8',
  });

  const nodeTextContent = fs.readFileSync(`${RAW_DATA_PATH}/intermediate_vertexes_${floor}.xml`, {
    encoding: 'utf8',
  });

  const roomTextContent = fs.readFileSync(`${RAW_DATA_PATH}/main_vertexes_${floor}.xml`, {
    encoding: 'utf8',
  });

  const connectors = connectorTextContent
    .split('\n')
    .filter(line => Boolean(line.trim()))
    .map(line => {
      const [id, nodeId, weight, floorIds] = line.split(';');
      return compactObject({
        id: id.replace('NAB', 'LSK'),
        nodeId,
        weight: parseInt(weight, 10),
        floorIds: floorIds
          .split(',')
          .map(v => v.trim())
          .map(v => v.replace('NAB', 'LSK'))
          .filter(v => v),
      });
    });

  const connectorsByNodeId = keyBy(connectors, o => o.nodeId);

  const polygonsByRoomId = subAreaTextContent
    .split('\n')
    .filter(line => Boolean(line.trim()))
    .map(line => {
      const [, roomId, polygon] = line.split(';');
      return {
        roomId,
        polygon: polygon
          .split('|')
          .map(coordinates => coordinates.split(',').map(v => parseInt(v, 10))),
      };
    })
    .reduce(
      (polygonsByRoomIdAccumulator, { roomId, polygon }) => ({
        ...polygonsByRoomIdAccumulator,
        [roomId]: [...(polygonsByRoomIdAccumulator[roomId] || []), polygon],
      }),
      {},
    );

  const rooms = roomTextContent
    .split('\n')
    .filter(line => Boolean(line.trim()))
    .map(line => {
      const [id, name, type, keywords, polygon, imageUrl, url] = line.split(';');

      const mainPolygon = polygon
        .split('|')
        .map(coordinates => coordinates.split(',').map(v => parseInt(v, 10)));

      return compactObject({
        id,
        name: name.trim(),
        tagIds: getTagIdsFromType(type),
        keywords: keywords
          .split(',')
          .map(v => v.trim())
          .filter(v => v && v !== 'null'),
        geoLocs: {
          type: 'MultiPolygon',
          coordinates: [
            [mainPolygon],
            ...(polygonsByRoomId[id] || []).map(innerRing => [innerRing]),
          ],
        },
        imageUrl: imageUrl.trim(),
        url: url.trim(),
        unsearchable: type === 'hide from searching',
      });
    });

  const roomsById = keyBy(rooms, o => o.id);

  // To clean existing room data with two or more doors attached
  const roomIdUsed = {};

  const nodes = nodeTextContent
    .split('\n')
    .filter(line => Boolean(line.trim()))
    .map(line => {
      const [id, coordinates, roomId] = line.split(';');
      const obj = compactObject({
        coordinates: coordinates.split(',').map(v => parseInt(v, 10)),
        ...((!roomIdUsed[roomId] && roomsById[roomId]) || {}),
        connectorId: (connectorsByNodeId[id] || {}).id,
        floorId: floor.replace('NAB', 'LSK'),
        _id: shortid(),
        id,
      });

      roomIdUsed[roomId] = true;
      return obj;
    });

  // rooms without nodes attached
  rooms.forEach(room => {
    if (roomIdUsed[room.id]) {
      return;
    }

    console.log(`Orphan room ${room.name}, ${room.id} at floor ${floor} created`);
    nodes.push({
      ...room,
      floorId: floor.replace('NAB', 'LSK'),
      _id: shortid(),
      id: null,
    });
  });

  const nodesById = keyBy(nodes, o => o.id);

  const edges = edgeTextContent
    .split('\n')
    .filter(line => Boolean(line.trim()))
    .map(line => {
      const [id, fromNodeId, toNodeId, , , weight] = line.split(';');
      if (id === 'null') {
        return null;
      }

      return {
        _id: shortid(),
        id,
        fromNodeId: nodesById[fromNodeId]._id,
        toNodeId: nodesById[toNodeId]._id,
        weightType: parseInt(weight, 10) === 0 ? 'max' : 'nodeDistance',
        floorId: floor.replace('NAB', 'LSK'),
      };
    })
    .filter(v => v);

  // eslint-disable-next-line no-restricted-syntax
  for (const node of nodes) {
    const { id, ...others } = node;
    try {
      // eslint-disable-next-line no-await-in-loop
      await nodesModel.insertOne(others);
      console.log(`Inserted node ${id} with _id ${others._id}`);
    } catch (err) {
      console.error(`Error inserting node ${id} with _id ${others._id}`, err);
    }
  }

  // eslint-disable-next-line no-restricted-syntax
  for (const edge of edges) {
    const { id, ...others } = edge;
    try {
      // eslint-disable-next-line no-await-in-loop
      await edgesModel.insertOne(others);
      console.log(`Inserted edge ${id}`);
    } catch (err) {
      console.error(`Error inserting edge ${id}`, err);
    }
  }
}

async function migrate() {
  const floorIds = Object.keys(floorsData);

  // eslint-disable-next-line no-restricted-syntax
  for (const floorId of floorIds) {
    console.log(`Migrating nodes and edges for floor ${floorId}`);
    try {
      // eslint-disable-next-line no-await-in-loop
      await migrateFloor(floorId);
    } catch (err) {
      console.log(`Error while migrate floor ${floorId}`, err);
    }
  }

  console.log('Done');
  process.exit();
}

migrate();
