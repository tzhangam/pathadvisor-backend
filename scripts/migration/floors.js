'use strict';

const { RAW_DATA_PATH } = process.env;
const floors = require('../../models/floors');

// eslint-disable-next-line
const floorsData = require(`${process.cwd()}/${RAW_DATA_PATH}/floors.js`);

async function migrate() {
  await Promise.all(
    Object.keys(floorsData).map(async floorId => {
      try {
        const newFloorId = floorId.replace('NAB', 'LSK');
        await floors.insertOne({ ...floorsData[floorId], _id: newFloorId });
        console.log(`Inserted floor ${floorId} with _id ${newFloorId}`);
      } catch (err) {
        console.error(`Error inserting floor with _id ${floorId}`, err);
      }
    }),
  );

  console.log('Done');
  process.exit();
}

migrate();
