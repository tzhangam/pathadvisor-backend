'use strict';

const { RAW_DATA_PATH } = process.env;
const tags = require('../../models/tags');

// eslint-disable-next-line
const tagsData = require(`${process.cwd()}/${RAW_DATA_PATH}/tags.js`);

async function migrate() {
  await Promise.all(
    tagsData.map(async ({ id, ...others }) => {
      try {
        await tags.insertOne({ ...others, _id: id });
        console.log(`Inserted tag with _id ${id}`);
      } catch (err) {
        console.error(`Error inserting floor with _id ${id}`, err);
      }
    }),
  );

  console.log('Done');
  process.exit();
}

migrate();
