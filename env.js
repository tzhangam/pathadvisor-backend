'use strict';

// Mostly from https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/config/env.js

const fs = require('fs');
const dotenv = require('dotenv');

const ENV_FILE_PATH = '.env';

const NODE_ENV = process.env.NODE_ENV;
if (!NODE_ENV) {
  throw new Error('The NODE_ENV environment variable is required but was not specified.');
}

module.exports = function loadEnv() {
  // https://github.com/bkeepers/dotenv#what-other-env-files-can-i-use
  const dotenvFiles = [
    `${ENV_FILE_PATH}.${NODE_ENV}.local`,
    `${ENV_FILE_PATH}.${NODE_ENV}`,
    // Don't include `.env.local` for `test` environment
    // since normally you expect tests to produce the same
    // results for everyone
    NODE_ENV !== 'test' && `${ENV_FILE_PATH}.local`,
    ENV_FILE_PATH,
  ].filter(Boolean);

  // Load environment variables from .env* files. Suppress warnings using silent
  // if this file is missing. dotenv will never modify any environment variables
  // that have already been set.  Variable expansion is supported in .env files.
  // https://github.com/motdotla/dotenv
  dotenvFiles.forEach(dotenvFile => {
    if (fs.existsSync(dotenvFile)) {
      dotenv.config({
        path: dotenvFile,
      });
    }
  });
};
