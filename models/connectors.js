'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['_id', 'floorIds'],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 1,
    },
    floorIds: {
      $id: '#/properties/floorIds',
      type: 'array',
      items: {
        $id: '#/properties/floorIds/items',
        type: 'string',
        minLength: 1,
      },
    },
    weight: {
      $id: '#/properties/weight',
      type: 'integer',
    },
    tagIds: {
      $id: '#/properties/tagIds',
      type: 'array',
      items: {
        $id: '#/properties/tagIds/items',
        type: 'string',
        minLength: 1,
      },
    },
    ignoreMinLiftRestriction: {
      $id: '#/properties/ignoreMinLiftRestriction',
      type: 'boolean',
    },
  },
};

const COLLECTION_NAME = 'connectors';

module.exports = createModel(COLLECTION_NAME, { schema });
