'use strict';

const createModel = require('./base/createModel');

const schema = {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['ipAddress', 'searchMode', 'from', 'to', 'datetime', 'platform'],
  properties: {
    ipAddress: {
      $id: '#/properties/ipAddress',
      type: 'string',
      format: 'ipv4',
    },
    referer: {
      $id: '#/properties/referer',
      type: 'string',
      minLength: 1,
    },
    userAgent: {
      $id: '#/properties/userAgent',
      type: 'string',
      minLength: 1,
    },
    searchMode: {
      $id: '#/properties/searchMode',
      type: 'string',
      enum: ['normal', 'quickCustom'],
    },
    from: {
      $id: '#/properties/from',
      type: 'string',
      minLength: 1,
    },
    to: {
      $id: '#/properties/to',
      type: 'string',
      minLength: 1,
    },
    datetime: {
      $id: '#/properties/datetime',
      type: 'integer',
    },
    platform: {
      $id: '#/properties/platform',
      type: 'string',
      enum: ['desktop', 'mobile'],
    },
  },
};

const COLLECTION_NAME = 'logs';

module.exports = createModel(COLLECTION_NAME, { schema });
