'use strict';

const Ajv = require('ajv');
const { MongoError } = require('mongodb');

const validator = new Ajv({ allErrors: true });
const getCollection = require('../../db/getCollection');
const ValidationError = require('../../errors/ValidationError');
const NotFoundError = require('../../errors/NotFoundError');

module.exports = function createModel(
  collectionName,
  {
    schema = {},
    updateSchema = { ...schema, required: [], minProperties: 1, if: undefined, then: undefined },
    transformInsertData,
    transformUpdateData,
  },
) {
  const validateInsert = validator.compile(schema);
  const validateUpdate = validator.compile(updateSchema);

  function validateInputData(data, validate) {
    if (!validate(data)) {
      const err = new ValidationError(validate.errors, 'Validation Error');
      throw err;
    }
  }

  async function insertOne(originalData) {
    const collection = await getCollection(collectionName);

    const data = transformInsertData ? transformInsertData(originalData) : originalData;
    validateInputData(data, validateInsert);

    try {
      const { ops, result } = await collection.insertOne(data);

      if (result.ok !== 1) {
        throw new MongoError(`insertOne did not operate correctly. result.ok is ${result.ok}`);
      }

      return ops[0];
    } catch (err) {
      if (!(err instanceof MongoError)) {
        throw err;
      }

      if (err.code !== 11000) {
        throw err;
      }

      throw new ValidationError(
        [{ dataPath: '._id', message: 'is duplicated' }],
        `Validation Error`,
      );
    }
  }

  async function updateOne(query, dataWithId) {
    const collection = await getCollection(collectionName);

    const { _id, ...originalData } = dataWithId;

    const data = transformUpdateData ? transformUpdateData(originalData) : originalData;
    validateInputData(data, validateUpdate);

    const {
      result: { ok },
      matchedCount,
    } = await collection.updateOne(query, { $set: data });

    if (ok !== 1) {
      throw new MongoError(`updateOne did not operate correctly. result.ok is ${ok}`);
    }

    if (matchedCount < 1) {
      throw new NotFoundError(query, `Document not found`);
    }

    return data;
  }

  async function updateOneNoValidate(query, update) {
    const collection = await getCollection(collectionName);

    const {
      result: { ok },
      matchedCount,
    } = await collection.updateOne(query, update);

    if (ok !== 1) {
      throw new MongoError(`updateOne did not operate correctly. result.ok is ${ok}`);
    }

    if (matchedCount < 1) {
      throw new NotFoundError(query, `Document not found`);
    }

    return update;
  }

  async function deleteOne(query) {
    const collection = await getCollection(collectionName);
    const { result, deletedCount } = await collection.deleteOne(query);

    if (result.ok !== 1) {
      throw new MongoError(`deleteOne did not operate correctly. result.ok is ${result.ok}`);
    }

    if (deletedCount !== 1) {
      throw new NotFoundError(query, `Document not found`);
    }
  }

  async function find(filter, options) {
    const collection = await getCollection(collectionName);
    return collection.find(filter, options);
  }

  async function findOne(query, options) {
    const collection = await getCollection(collectionName);
    return collection.findOne(query, options);
  }

  async function createIndex(fieldOrSpec, options) {
    const collection = await getCollection(collectionName);
    return collection.createIndex(fieldOrSpec, options);
  }

  return {
    insertOne,
    updateOne,
    updateOneNoValidate,
    deleteOne,
    find,
    findOne,
    createIndex,
  };
};
