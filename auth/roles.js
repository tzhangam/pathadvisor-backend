'use strict';

const permissions = require('./permissions');

const roles = {
  admin: {
    permissions,
  },
};

module.exports = roles;
